/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2011-2018 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "twoPhaseDragMultiplierModel.H"

// * * * * * * * * * * * * * * * * Selector  * * * * * * * * * * * * * * * * //

Foam::autoPtr<Foam::twoPhaseDragMultiplierModel> 
Foam::twoPhaseDragMultiplierModel::New
(
    const fvMesh& mesh,
    const dictionary& dict,
    const objectRegistry& objReg
)
{
    word type(dict.lookup("type"));

    Info<< "Selecting twoPhaseDragMultiplierModel: " 
        << type << endl;

    twoPhaseDragMultiplierModelsConstructorTable::iterator 
        cstrIter = 
            twoPhaseDragMultiplierModelsConstructorTablePtr_->find(type);

    if 
    (
        cstrIter 
        == 
        twoPhaseDragMultiplierModelsConstructorTablePtr_->end()
    )
    {
        FatalErrorInFunction
            << "Unknown twoPhaseDragMultiplierModel type "
            << type << endl << endl
            << "Valid twoPhaseDragMultiplierModel types are : " 
            << endl
            << twoPhaseDragMultiplierModelsConstructorTablePtr_
            ->sortedToc()
            << exit(FatalError);
    }

    return cstrIter()(mesh, dict, objReg);
}

// ************************************************************************* //

### IMPORTS

import sys
import matplotlib.pyplot as plt

### FUNCTIONS

def plotEntryToAx(lines, key, pos, xAxis, plotType, lab) :

    entryValues = []
    for i in range(len(lines)) :
        if key in lines[i] :
            entry = lines[i].split()[pos]
            entry = entry.split(")")[0]
            entry = entry.split("(")[0]
            entryValues.append(abs(float(entry)))

    lenX = len(xAxis)
    lenE = len(entryValues)
    newXAxis = []
    newEntryValues = []
    if lenX > lenE :
        for i in range(len(entryValues)) :
            newXAxis.append(xAxis[i])
        newEntryValues = entryValues
    elif lenX < lenE :
        for i in range(len(xAxis)) :
            newEntryValues.append(entryValues[i])
        newXAxis = xAxis
    else :
        newXAxis = xAxis
        newEntryValues = entryValues
    plotType(newXAxis, newEntryValues, label=lab)  

def axesGridLegend(axes) :
    for ax in axes :
        ax.grid(True)
        ax.legend(fontsize=6)
    axes[-1].set_xlabel("time (s)")

### MAIN

fig, ax1 = plt.subplots(1)
axes = [ax1]
ax1.set_ylabel("contErr (kg/m3)")

for q in range(len(sys.argv)-1) :

    # Read log lines
    filename = sys.argv[q+1]
    file = open(filename, "r")
    lines = file.readlines()

    # Read times
    times = []
    for line in lines :
        if "Time =" in line and not "ExecutionTime" in line :
            time = float(line.split()[2])
            times.append(time)

    plotEntryToAx(lines, "Cumulative continuity errors", 6, times, ax1.semilogy, filename+" liquid")    
    plotEntryToAx(lines, "Cumulative continuity errors", 7, times, ax1.semilogy, filename+" vapour")
    plotEntryToAx(lines, "Cumulative dm", 6, times, ax1.semilogy, filename+" dmLoss")

axesGridLegend(axes)
plt.legend()
plt.show()

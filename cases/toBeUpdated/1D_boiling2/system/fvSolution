/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  6
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    p_rgh
    {
        solver          GAMG;
        smoother        DIC;
        tolerance       1e-6;
        relTol          0;
    }

    p_rghFinal
    {
        $p_rgh;
        relTol          0;
    }

    "e.*"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-6;
        relTol          0;
        minIter         1;
    }

    "h.*"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-6;
        relTol          0;
        minIter         1;
    }

    ".*"
    {
        solver          PBiCGStab;
        preconditioner  diagonal;
        tolerance       1e-06;
        relTol          0.001;
    }

    alpha
    {
        adjustSubCycles     true;
        alphaMaxCo          0.25;
    }
}

PIMPLE
{
    nOuterCorrectors            8;
    nCorrectors                 8;
    correctUntilConvergence     true;
    nNonOrthogonalCorrectors    0;
    partialEliminationMode      implicit;
    momentumMode                faceCentered;
    oscillationLimiterFraction  0;
    adaptiveMassTransferLimiter true;
    enthalpyStabilizationMode   source;
}

relaxationFactors
{
    equations
    {
        ".*"            1;
        "h.vapour"      0.5;
    }

    fields
    {
        "dmdt"              0.25;
    }
}


// ************************************************************************* //

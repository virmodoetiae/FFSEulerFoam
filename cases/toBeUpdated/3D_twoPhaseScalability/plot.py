### IMPORTS

import sys
import matplotlib.pyplot as plt

### FUNCTIONS

def sum(x) :

    n = len(x)
    sumX = 0
    for i in range(n) :
        sumX += x[i]
    return sumX

def limit(x, y) :
    
    tmpX = []
    i = 0
    while len(tmpX) < len(y) :
        tmpX.append(x[i])
        i += 1
    return tmpX

def filter(x, s, averageMode) :

    tmpX = []
    n = len(x)
    i = 0
    while i < n :
        j = i
        avg = 0
        ss = 0
        while j < i + s :
            try :
                avg += x[j]
                ss += 1
                j += 1
            except :
                break
        if averageMode :
            avg /= ss
        elif ss < s :
            avg *= s/ss
        j = i
        while j < i + ss :
            tmpX.append(avg)
            j += 1
        i = j
    return tmpX

### MAIN

filterSize = 10

fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)

ax1.set_ylabel("Residual (-)")
ax2.set_ylabel("p iters / "+str(filterSize)+" deltaT (-)")
ax3.set_ylabel("runTime (s)")
ax3.set_xlabel("time (s)")

nArgs = len(sys.argv)-1

for q in range(nArgs) :

    filename = sys.argv[q+1]
    file = open(filename, "r")
    lines = file.readlines()
    n = 0;
    for line in lines :
        if "PIMPLE: iteration" in line :
            nn = int(line.split()[2])
            if nn > n :
                n = nn
            else :
                break
    
    times = []
    runTimes = []
    
    pEqnIters = 0
    regimeMapTime = 0
    regimeModelsTime = 0
    alphaEqnsTime = 0
    pEqnTime = 0
    EEqnsTime = 0
    nSubCycles = 0
    maxCo = 0
    maxUrCo = 0

    pEqnIterss = []
    regimeMapTimes = []
    regimeModelsTimes = []
    alphaEqnsTimes = []
    pEqnTimes = []
    EEqnsTimes = []
    nSubCycless = []
    maxCos = []
    
    initialResiduals = []
    
    readResidual = False
    firstTime = True
    
    for line in lines :
        if "Courant Number (" in line:
            maxCo = float(line.split()[6])
        elif "Max Ur" in line :
            maxUrCo = float(line.split()[5])
        elif "Time = " in line :
            splitLine = line.split()
            if len(splitLine) == 3 :
                time = float(splitLine[2])
                if not firstTime :
                    times.append(time)
                    runTime = regimeMapTime + regimeModelsTime + alphaEqnsTime + pEqnTime + EEqnsTime
                    runTimes.append(runTime)
                    maxCos.append(max(maxCo, maxUrCo))
                    maxCo = 0
                    maxUrCo = 0
                    pEqnIterss.append(pEqnIters)
                    pEqnIters = 0
                    regimeMapTimes.append(regimeMapTime) 
                    regimeMapTime = 0
                    regimeModelsTimes.append(regimeModelsTime)
                    regimeModelsTime = 0
                    alphaEqnsTimes.append(alphaEqnsTime)
                    alphaEqnsTime = 0
                    pEqnTimes.append(pEqnTime)
                    pEqnTime = 0
                    EEqnsTimes.append(EEqnsTime)
                    EEqnsTime = 0
                    nSubCycless.append(nSubCycles)
                    nSubCycles = 0
            else :
                firstTime = False
        elif "PIMPLE: iteration "+str(n) in line :
            readResidual = True
        elif "Solving for p_rgh" in line :
            splitLine = line.split()
            pEqnIters += int(splitLine[14])
            if readResidual :
                residual = float(splitLine[7].split(",")[0])
                initialResiduals.append(residual)
                readResidual = False
        elif "regime map" in line :
            regimeMapTime += float(line.split()[5])
        elif "regime models" in line :
            regimeModelsTime += float(line.split()[5])
        elif "alphaEqns took" in line :
            alphaEqnsTime += float(line.split()[3])
        elif "pEqn-UEqns took" in line :
            pEqnTime += float(line.split()[3])
        elif "EEqns took" in line :
            EEqnsTime += float(line.split()[3])
        elif "No SubCycles" in line :
            nSubCycles = float(line.split()[7])
    
    ax1.semilogy(limit(times, initialResiduals), initialResiduals, label=filename)
    
    fPEqnIterss = filter(pEqnIterss, filterSize, False)
    ax2.plot(limit(times, fPEqnIterss), fPEqnIterss, label=filename)
    
    fRunTimes = filter(runTimes, filterSize, True)
    ax3.plot(limit(times, fRunTimes), fRunTimes, label=filename)

    print(filename)
    print("  totTime =          ", round(sum(runTimes),3), " s")
    print("  alphaEqnsTime =    ", round(sum(alphaEqnsTimes),3), " s")
    print("  regimeMapTime =    ", round(sum(regimeMapTimes),3), " s")
    print("  regimeModelsTime = ", round(sum(regimeModelsTimes),3), " s")
    print("  pEqn-UEqns Time =  ", round(sum(pEqnTimes),3), " s")
    print("  EEqnsTime =        ", round(sum(EEqnsTimes),3), " s")
    print("  totPEqnIters = ", round(sum(pEqnIterss)))
    print("  totSubCycles = ", round(sum(nSubCycless)))
    print(" ")

if nArgs > 1:
    ax1.legend()
    ax2.legend()
    ax3.legend()
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)
plt.show()

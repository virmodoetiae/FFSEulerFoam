/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  6
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    p_rgh
    {
        solver          GAMG;
        smoother        DIC;
        tolerance       1e-8;
        relTol          0;
    }

    p_rghFinal
    {
        $p_rgh;
        relTol          0;
    }

    "e.*"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-8;
        relTol          0;
        minIter         1;
    }

    "h.*"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-8;
        relTol          0;
        minIter         1;
    }

    ".*"
    {
        solver          PBiCGStab;
        preconditioner  diagonal;
        tolerance       1e-06;
        relTol          0.001;
    }
}

PIMPLE
{
    nOuterCorrectors 6;
    nCorrectors      3;
    nNonOrthogonalCorrectors 0;
    partialEliminationMode      implicit;
    momentumMode                faceCentered;
    oscillationLimiterFraction  0;
}

relaxationFactors
{
    equations
    {
        ".*"                1.0;
        "epsilon"           0.9;
        "k"                 0.9;
    }

    fields
    {
        "dmdt"              0.2;
        "iT12"              0.05;
    }
}


// ************************************************************************* //

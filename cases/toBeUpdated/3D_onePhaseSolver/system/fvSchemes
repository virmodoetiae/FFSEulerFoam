/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  6
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSchemes;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

ddtSchemes
{
    default         Euler;
}

gradSchemes
{
    default         Gauss linear;
}

divSchemes
{
    default                                 none;

    div(phi,alpha)                          Gauss vanLeer;
    div(phir,alpha)                         Gauss vanLeer;

    div(phi,alpha.liquid)                   Gauss vanLeer;
    div(phir,alpha.vapour,alpha.liquid)     Gauss vanLeer;
    div(phir,alpha.structure,alpha.liquid)  Gauss vanLeer;
    div(phi,alpha.vapour)                   Gauss vanLeer;
    div(phir,alpha.liquid,alpha.vapour)     Gauss vanLeer;
    div(phir,alpha.structure,alpha.vapour)  Gauss vanLeer;
    div(alphaRhoPhi,k)  Gauss vanLeer;
    div(alphaRhoPhi,K)                      Gauss limitedLinear 1;

    "div\(alphaRhoPhi.*,U.*\)"              Gauss upwind;
    "div\(phi.*,U.*\)"                      Gauss upwind;

    "div\(alphaRhoPhi.*,T.*\)"              Gauss upwind;
    "div\(alphaRhoPhi.*,(h|e).*\)"          Gauss upwind;
    "div\(alphaRhoPhi.*,K.*\)"              Gauss upwind;
    "div\(alphaPhi.*,p\)"                   Gauss upwind;

    "div(alphaRhoPhiNu,U)" Gauss linear;
    //"div\(\(\(thermo:rho.*\*nuEff.*\)*dev2\(T\(grad\(U.*\)\)\)\)\)" Gauss linear;
}

laplacianSchemes
{
    default         Gauss linear uncorrected;
}

interpolationSchemes
{
    default         linear;
}

snGradSchemes
{
    default         uncorrected;
}


// ************************************************************************* //

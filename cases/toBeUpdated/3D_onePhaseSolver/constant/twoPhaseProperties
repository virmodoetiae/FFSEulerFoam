/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  6
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      phaseProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ------------------------------------------------------------------------- //
// --- THERMAL-HYDRAULIC TYPE ---------------------------------------------- //
// ------------------------------------------------------------------------- //

thermalHydraulicType    "twoPhase";

fluid1                  "liquid";
fluid2                  "vapour";

// ------------------------------------------------------------------------- //
// --- FLUID PROPERTIES ---------------------------------------------------- //
// ------------------------------------------------------------------------- //

liquidProperties
{
    residualAlpha   1e-5;
}

vapourProperties
{
    residualAlpha   1e-5;
    //thermoResidualAlpha 1e-0;
    initialAlphas
    {
        //"heated"    0.5;
        //"nonHeated" 0.9;
    }
}

// ------------------------------------------------------------------------- //
// --- STRUCTURES PROPERTIES ----------------------------------------------- //
// ------------------------------------------------------------------------- //

structureProperties
{    
    type            "byZone";

    "heated"
    {
        volumeFraction      0.75;//0.4720574811;
        Dh                  0.005362850651;
        localTortuosity     (0.1583 0.1583 1);
        powerModel
        {
            type                constantPower;
            iA                  314.7049873894;
            powerDensity        5E+08;
            rho                 7700;
            Cp                  500;
            T                   652.15;
        }
        passiveProperties
        {
            initialT        652.15;
            iA              0;
            rho             0;
            Cp              0;
        }
    }

    "nonHeated"
    {
        volumeFraction      0.5;//0.4720574811;
        Dh                  0.005362850651;
        localTortuosity     (0.1583 0.1583 1);
        powerModel
        {
            type            none;
        }
        passiveProperties
        {
            initialT        652.15;
            iA              314.7049873894;
            rho             7700;
            Cp              500;
        }
    }
}

// ------------------------------------------------------------------------- //
// --- REGIME MAP MODEL ---------------------------------------------------- //
// ------------------------------------------------------------------------- //

regimeMapModel
{
    type                oneParameter;
    parameter           "Re.liquid.structure";
    regimeBounds
    {
        "laminar"         (0       70000);
        "turbulent"       (100000    100001);
    }
}

// ------------------------------------------------------------------------- //
// --- REGIME PHYSICS FOR EACH REGIME -------------------------------------- //
// ------------------------------------------------------------------------- //

physicsModelsByRegime
{
    "laminar"
    {
        dragModels
        {
            "liquid.vapour" 
            {
                /*
                type constant;
                value 500000;
                */
                type Autruffe;
                vapourName "vapour";
                
            }

            "liquid.heated"
            {
                type Darcy;
                isotropic true;
                const (0 0 0);
                coeff (0.316 0.316 0.316);
                exp (-0.25 -0.25 -0.25);
            }
            "liquid.nonHeated"
            {
                type Darcy;
                isotropic true;
                const (0 0 0);
                coeff (0.316 0.316 0.316);
                exp (-0.25 -0.25 -0.25);
            }

            "vapour.heated"
            {
                type constant;
                value (1 0 0 0 1 0 0 0 1);
            }
            "vapour.nonHeated"
            {
                type constant;
                value (1 0 0 0 1 0 0 0 1);
            }
        }

        heatTransferModels
        {
            "liquid.vapour"
            {
                type    Nusselt;
                const   2;
                coeff   0.6;
                expRe   0.5;
                expPr   0.33;
            }
            "vapour.liquid"
            {
                type constant;
                value 100;
            }
            
            "liquid.heated"
            {
                //- By Mikityuk, 
                //  Nu = 0.047*(1-e^(-3.8*(x-1)))*(Pe^0.77+250)
                //  with x = pitch/diameter ratio of the bundle
                type    Nusselt;
                const   4;
                coeff   0;
                expRe   0;
                expPr   0;
            }
            "liquid.nonHeated"
            {
                //- By Mikityuk, 
                //  Nu = 0.047*(1-e^(-3.8*(x-1)))*(Pe^0.77+250)
                //  with x = pitch/diameter ratio of the bundle
                type    Nusselt;
                const   4;
                coeff   0;
                expRe   0;
                expPr   0;
            } 
            
            "vapour.heated"
            {
                type constant;
                value 100;
            }
            "vapour.nonHeated"
            {
                type constant;
                value 100;
            }   
        }

        fluidGeometry
        {
            dispersed   "vapour";

            dispersedFluidDiameterModel
            {
                type    constant;
                value   0.005362850651;
            }

            fluidInterfacialAreaModel
            {
                type        "Schor";
                alphaName   "vapour";
                P           0.0079;
                D           0.006;
            }

            structureInterfacialAreaPartitionModel
            {
                type        "constant";
                value1      1;
                value2      0;
            }
        }
    }

    "turbulent"
    {
        dragModels
        {
            "liquid.vapour" 
            {
                /*
                type constant;
                value 500000;
                */
                type Autruffe;
                vapourName "vapour";
                
            }

            "liquid.heated"
            {
                type Darcy;
                isotropic true;
                const (0 0 0);
                coeff (0.316 0.316 0.316);
                exp (-0.125 -0.125 -0.125);
            }
            "liquid.nonHeated"
            {
                type Darcy;
                isotropic true;
                const (0 0 0);
                coeff (0.316 0.316 0.316);
                exp (-0.125 -0.125 -0.125);
            }

            "vapour.heated"
            {
                type constant;
                value (1 0 0 0 1 0 0 0 1);
            }
            "vapour.nonHeated"
            {
                type constant;
                value (1 0 0 0 1 0 0 0 1);
            }
        }

        heatTransferModels
        {
            "liquid.vapour"
            {
                type    Nusselt;
                const   2;
                coeff   0.6;
                expRe   0.5;
                expPr   0.33;
            }
            "vapour.liquid"
            {
                type constant;
                value 100;
            }
            
            "liquid.heated"
            {
                //- By Mikityuk, 
                //  Nu = 0.047*(1-e^(-3.8*(x-1)))*(Pe^0.77+250)
                //  with x = pitch/diameter ratio of the bundle
                type    Nusselt;
                const   7.0784174838;
                coeff   0.028;
                expRe   0.77;
                expPr   0.77;
            }
            "liquid.nonHeated"
            {
                //- By Mikityuk, 
                //  Nu = 0.047*(1-e^(-3.8*(x-1)))*(Pe^0.77+250)
                //  with x = pitch/diameter ratio of the bundle
                type    Nusselt;
                const   7.0784174838;
                coeff   0.028;
                expRe   0.77;
                expPr   0.77;
            } 
            
            "vapour.heated"
            {
                type constant;
                value 100;
            }
            "vapour.nonHeated"
            {
                type constant;
                value 100;
            }   
        }

        fluidGeometry
        {
            dispersed   "vapour";

            dispersedFluidDiameterModel
            {
                type    constant;
                value   0.005362850651;
            }

            fluidInterfacialAreaModel
            {
                type        "Schor";
                alphaName   "vapour";
                P           0.0079;
                D           0.006;
            }

            structureInterfacialAreaPartitionModel
            {
                type        "constant";
                value1      1;
                value2      0;
            }
        }
    }
}

// ------------------------------------------------------------------------- //
// --- PHASE-CHANGE RELATED ------------------------------------------------ //
// ------------------------------------------------------------------------- //

phaseChangeModel    "none";

saturationModel
{
    type            "BrowningPotter";
}

// ------------------------------------------------------------------------- //
// --- MISCELLANEA --------------------------------------------------------- //
// ------------------------------------------------------------------------- //

pMin                    10000;
pRefCell                0;
pRefValue               100000; //104500;
residualKd              1;

// ************************************************************************* //
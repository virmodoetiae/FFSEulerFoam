### Solves a steady state 1-D heat equation for an axial-symmetric hollow 
### cylinder via a Finite Difference discretisation. Boundary conditions are 
### zeroGradient for the inner cylinder surface and a covective BC for the 
### outer surface. A custom volumetric heat source can be specified in the 
### domain. Constant material properties.

###############################################################################
### USER SELECTABLE PART ######################################################
###############################################################################

rfi = 	0	#0.0#0.001560
rfo =   5e-3	#0.004680
rci =   5.6e-3	#0.004835
rco =   6e-3	#0.005335
kf = 	3
kc = 	20
qDot = 	2e8 # W/m3
hGap = 	3000 # W/m2/K
hf = 	10000 # W/m2/K
Tf = 	683.647 # K

Ns = [
	[21, 7]
]

useFixedValue = False 

###############################################################################
### END OF USER SELECTABLE PART ###############################################
###############################################################################

### IMPORTS

import math
import numpy as np
from matplotlib import pyplot as plt

### CLASSES

### FUNCTIONS

def solveForMeshSize(Ns) :

	# Init mesh
	Nf = Ns[0]
	Nc = Ns[1]
	N = Nf+Nc
	drf = (rfo-rfi)/(Nf-1)
	drc = (rco-rci)/(Nc-1)
	drg = rci-rfo
	r = [rfi]
	for i in range(Nf-1) :
		r.append(r[-1]+drf)
	r.append(r[-1]+drg)
	for i in range(Nc-1) :
		r.append(r[-1]+drc)

	# Init matrix, source
	b = np.empty([N, 1])
	M = np.empty([N, N])
	for row in range(len(M)) :
		b[row][0] = 0
		for col in range(len(M[row])) :
			M[row][col] = 0

	### Build matrix

	# Fuel inner surface, zero gradient
	ir2 = 1.0/(drf**2)
	M[0][1] = 	-2*ir2*kf
	M[0][0] = 	2*ir2*kf
	b[0] = 		qDot

	# Fuel bulk
	for i in range(1, Nf-1) :
		ir2 = 1/(drf**2)
		M[i][i+1] = 	-kf*(ir2+1.0/(2*r[i]*drf)) 
		M[i][i-1] =		-kf*(ir2-1.0/(2*r[i]*drf))
		M[i][i] = 		-M[i][i+1]-M[i][i-1];
		b[i] = qDot

	# Fuel outer surface, "convective" BC with cladding inner surface
	i = Nf-1
	ir2 = 1.0/(drf**2)
	A = kf*ir2
	B = kf/(2*rfo*drf)
	C = 2*drf*hGap/kf
	M[i][i+1] = 	-C*(A+B)
	M[i][i-1] =		-2*A
	M[i][i] = 		-M[i][i+1]-M[i][i-1];
	b[i] = qDot

	# Cladding inner surface, "convective" BC with fuel outer surface
	i = Nf
	ir2 = 1.0/(drc**2)
	A = kc*ir2
	B = kc/(2*rci*drc)
	C = (2*drc*hGap/kc)*(rfo/(rci+drc)) # Adjust h so to preserve heat flow
	M[i][i-1] = 	-C*(A+B)
	M[i][i+1] =		-2*A
	M[i][i] = 		-M[i][i+1]-M[i][i-1];
	b[i] = 0

	# Cladding bulk
	for i in range(Nf+1, N-1) :
		ir2 = 1.0/(drc**2)
		M[i][i+1] = 	-kc*(ir2+1.0/(2*r[i]*drc))
		M[i][i-1] =		-kc*(ir2-1.0/(2*r[i]*drc))
		M[i][i] = 		-M[i][i+1]-M[i][i-1];
		b[i] = 0

	# Cladding outer surface
	if useFixedValue :
		M[N-1][N-1] = 1
		b[N-1] = Tf
	else :
		ir2 = 1.0/(drc**2)
		A = kc*ir2
		B = kc/(2*rco*drc)
		C = 2*drc*hf/kc	
		M[N-1][N-2] = 	-2*A
		M[N-1][N-1] = 	2*A + C*(A+B)
		b[N-1] = 		C*Tf*(A+B)

	# Solve
	T = np.linalg.solve(M, b)

	plt.plot(r, T, label=str(Nf)+"_"+str(Nc))

	print("Nf: "+str(Nf)+" Nc: "+str(Nc))
	aLP = qDot*math.pi*(rfo**2-rfi**2)
	print("    - analytical LP: 	"+str(aLP)+" W/m")
	nLP = (T[-1][0]-Tf)*hf*2*math.pi*rco
	print("    - numerical LPs:	    "+str(nLP)+" W/m (cladding)")
	nLPg = (T[Nf][0]-T[Nf-1][0])*hGap*2*math.pi*rfo
	print("    - numerical LPs:	    "+str(nLP)+" W/m (gap)")

	for i in range(N) :
		print(T[i][0])
	

### MAIN

for N in Ns :
	solveForMeshSize(N)
plt.grid(True)
plt.legend(loc="best")
plt.show()
### IMPORTS

import sys
import matplotlib.pyplot as plt

### MAIN

fig, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)

ax1.set_ylabel("finalResidual(-)")
ax2.set_ylabel("initialResidual(-)")
ax3.set_ylabel("pEqnIters")
ax3.set_xlabel("time(s)")

for q in range(len(sys.argv)-1) :

    filename = sys.argv[q+1]
    file = open(filename, "r")
    lines = file.readlines()

    n = 0;
    for line in lines :
        if "PIMPLE: iteration" in line :
            nn = int(line.split()[2])
            if nn > n :
                n = nn
            else :
                break

    times = []
    initialResiduals = []
    finalResiduals = []
    allResiduals = []
    pEqnIterss = []
    i = 0
    time = 0
    residual = 0
    initialResidual = 0
    pEqnIters = 0
    initialResidualFlag = False

    for line in lines :
        if  "PIMPLE: iteration "+str(n) in line :
            initialResidualFlag = True
        if "GAMG:  Solving for" in line :
            residual = float(line.split()[7].split(",")[0])
            allResiduals.append(residual)
            pEqnIters += float(line.split()[14])
            if initialResidualFlag :
                initialResidualFlag = False
                initialResidual = residual
        if "Time =" in line and not "ExecutionTime" in line :
            time = float(line.split()[2])
            times.append(time)
            finalResiduals.append(residual)
            initialResiduals.append(initialResidual)
            pEqnIterss.append(pEqnIters)
            pEqnIters = 0

    del initialResiduals[0]
    del finalResiduals[0]
    del pEqnIterss[0]
    t0 = times[0]
    for i in range(len(times)) :
        times[i] = times[i] - t0  
    del times[-1]
    ax1.semilogy(times, finalResiduals, label=filename)
    ax2.semilogy(times, initialResiduals, label=filename)
    ax3.semilogy(times, pEqnIterss, label=filename)
ax1.legend()
ax1.grid(True)
ax2.legend()
ax2.grid(True)
ax3.legend()
ax3.grid(True)
plt.show()

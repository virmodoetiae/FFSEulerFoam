'''

Notes regarding reference frames:
    The wedge is built by default in the following reference frame:

    ^ Z 
    |
    |   
    o--- > X
    Y

    where the Y axis is oriented TOWARDS the inside of the monitor plane
    The wedge axis is alined to the z axis and the wedge symmetry plane
    coincides with the XZ plane.

Notes on face indexing:
    In order to aid the construction of block faces, 6 directions are defined
    and indexed by a label in the following way:
      Label    Direction
        0        + X
        1        - X
        2        + Z
        3        - Z
        4        + Y
        5        - Y

'''

###############################################################################
### IMPORTS
###############################################################################

import os
import re
import sys
import math
import time



###############################################################################
### GLOBAL
###############################################################################

'''
blockTypeList = list of the block types implemented in wedgeDict
blockList     = un-ordered list of blocks that compose the layout 
                in wedgeLayout
blockLayout   = same as blockList, but the blocks are arranged in a 
                matrix that reflects their layout in wedgeLayout
'''

global origin, verteList, blockTypeList, blockList, blockLayout

blockTypeList = []
blockList     = []
blockLayout   = []
vertexList    = []



###############################################################################
### CLASSES
###############################################################################

class blockTypeClass :

    def __init__(self, label, zone, deltaX, deltaZ, nX, nZ, tmpFL) :
        self.label = label
        self.zone = zone
        self.deltaZ = deltaZ
        self.deltaX = deltaX
        self.nX = nX
        self.nZ = nZ
        self.tmpFaceList = tmpFL



class blockClass(blockTypeClass) :

    def __init__(self, bType, row, col) :
        blockTypeClass.__init__(self, bType.label, bType.zone, bType.deltaX, bType.deltaZ, bType.nX, bType.nZ, bType.tmpFaceList)
        self.row = row
        self.col = col
        self.vertexList = []
        self.faceList = []

    def __eq__(self, rhs) :
        if self.row == rhs.row and self.col == rhs.col :
            return True
        else :
            return False

    def __ne__(self, rhs) :
        if self == rhs :
            return False
        else :
            return True

    def string(self) :
        vertexListStr = "("
        for vertex in self.vertexList :
            vertexListStr += " "+str(vertex.label)
        vertexListStr += " )"
        blockMeshNodesStr = "("+str(self.nX)+" 1 "+str(self.nZ)+")"
        return "hex "+vertexListStr+" "+self.zone+" "+blockMeshNodesStr+" simpleGrading (1 1 1)"

    # The offsets are the physical coordinates of the block-belonging
    # point with the smallest z and r coordinates
    def createVerticesAndFaces(self, x0, y0, z0, vList) :
        x1 = x0 + self.deltaX
        z1 = z0 + self.deltaZ

        # Create the 8 points defining the hexahedral block
        v0 = vertexClass(x0, y0-math.tan(wedgeAngle)*x0, z0)
        v1 = vertexClass(x1, y0-math.tan(wedgeAngle)*x1, z0)
        v2 = vertexClass(x1, y0+math.tan(wedgeAngle)*x1, z0)
        v3 = vertexClass(x0, y0+math.tan(wedgeAngle)*x0, z0)
        v4 = vertexClass(x0, y0-math.tan(wedgeAngle)*x0, z1)
        v5 = vertexClass(x1, y0-math.tan(wedgeAngle)*x1, z1)
        v6 = vertexClass(x1, y0+math.tan(wedgeAngle)*x1, z1)
        v7 = vertexClass(x0, y0+math.tan(wedgeAngle)*x0, z1)

        # Add the points to the specified point list. The point
        # labels are updated accordingly. If a duplicate point
        # already exists in the list, it is not added, yet the
        # label of the point is set to the label of the existing
        # point
        v0.addToList(vList)
        v1.addToList(vList)
        v2.addToList(vList)
        v3.addToList(vList)
        v4.addToList(vList)
        v5.addToList(vList)
        v6.addToList(vList)
        v7.addToList(vList)

        # Register the points that make up the block in the 
        # blockPointList, referencing them through their labels.
        # Thus any issues arising from possible duplicate points
        # are resolved
        self.vertexList.append(v0)
        self.vertexList.append(v1)
        self.vertexList.append(v2)
        self.vertexList.append(v3)
        self.vertexList.append(v4)
        self.vertexList.append(v5)
        self.vertexList.append(v6)
        self.vertexList.append(v7)

        # Create faces with the created vertices
        fPosX = faceClass([v1, v5, v6, v2], self.tmpFaceList[0].patchName, self.tmpFaceList[0].isBaffle)
        if v0 != v3 and v4 != v7 :
            fNegX = faceClass([v0, v3, v7, v4], self.tmpFaceList[1].patchName, self.tmpFaceList[1].isBaffle)
        fPosZ = faceClass([v4, v7, v6, v5], self.tmpFaceList[2].patchName, self.tmpFaceList[2].isBaffle)
        fNegZ = faceClass([v0, v1, v2, v3], self.tmpFaceList[3].patchName, self.tmpFaceList[3].isBaffle)
        fPosY = faceClass([v2, v6, v7, v3], "wedgeY+", False)
        fNegY = faceClass([v0, v4, v5, v1], "wedgeY-", False)
        
        self.faceList.append(fPosX)
        if v0 != v3 and v4 != v7 : #i.e. if the block has all of the 6 sides and not just 5
            self.faceList.append(fNegX)
        self.faceList.append(fPosZ)
        self.faceList.append(fNegZ)
        self.faceList.append(fPosY)
        self.faceList.append(fNegY)



class vertexClass :

    def __init__(self, x, y, z) :
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
        self.label = ''

    def __eq__(self, rhs) :
        tol = 1e-6
        if abs(self.x - rhs.x) < tol and abs(self.y - rhs.y) < tol and abs(self.z - rhs.z) < tol :
            return True
        else :
            return False

    def __ne__(self, rhs) :
        if self == rhs :
            return False
        else :
            return True

    def __add__(self, rhs) :
        return vertexClass(self.x + rhs.x, self.y + rhs.y, self.z + rhs.z)

    def __neg__(self) :
        return vertexClass(-self.x, -self.y, -self.z)

    def __sub__(self, rhs) :
        return (self+(-rhs))

    def string(self) :
        return "( "+str(self.x)+" "+str(self.y)+" "+str(self.z)+" )"

    def searchIn(self, vList) :
        if len(vList) == 0 :
            return -1
        else :   
            for i, vertex in enumerate(vList) :
                if self == vertex :
                    return i
            return -1

    def addToList(self, vList) :
        label = self.searchIn(vList)
        if label == -1 :
            label = len(vList)
            self.label = label
            vList.append(self)
        else :
            self.label = label



class faceClass :

    def __init__(self, vList, pName, isBaffle) :
        self.vertexList = vList
        self.patchName = pName
        self.isInternal = False
        self.isBaffle = isBaffle

        if len(vList) > 0 :
            self.setBoundingBox()

    def __eq__(self, rhs) :
        for vertex in self.vertexList :
            if vertex not in rhs.vertexList :
                return False
        return True

    def __ne__(self, rhs) :
        if self == rhs :
            return False
        else :
            return True

    def setBoundingBox(self) :
        self.BB = []
        tol = 1e-4
        
        X = [vertex.x for vertex in self.vertexList]
        Y = [vertex.y for vertex in self.vertexList]
        Z = [vertex.z for vertex in self.vertexList]
        minX = min(X)
        maxX = max(X)
        minY = min(Y)
        maxY = max(Y)
        minZ = min(Z)
        maxZ = max(Z)

        self.BB.append(minX-tol)
        self.BB.append(maxX+tol)
        self.BB.append(minY-tol)
        self.BB.append(maxY+tol)
        self.BB.append(minZ-tol)
        self.BB.append(maxZ+tol)

    def string(self) :
        return "( "+str(self.vertexList[0].label)+" "+str(self.vertexList[1].label)+" "+str(self.vertexList[2].label)+" "+str(self.vertexList[3].label)+" )"

    def stringBB(self) :
        return "box ("+str(self.BB[0])+" "+str(self.BB[2])+" "+str(self.BB[4])+")("+str(self.BB[1])+" "+str(self.BB[3])+" "+str(self.BB[5])+");"



class patchClass :

    def __init__(self, pName, pType, fList) :
        self.patchName = str(pName)
        self.patchType = str(pType)
        self.faceList = fList

    def strings(self) :
        indent = "    "
        strings = []
        strings.append(indent+self.patchName+"\n")
        strings.append(indent+"{\n")
        strings.append(indent+indent+"type "+self.patchType+";\n")
        strings.append(indent+indent+"faces\n")
        strings.append(indent+indent+"(\n")
        for face in self.faceList:
            strings.append(indent+indent+indent+face.string()+"\n")
        strings.append(indent+indent+");\n")
        strings.append(indent+"}")

        return strings



class blockMeshDictClass :

    def __init__(self, vertices, blocks) :
        self.filename = "blockMeshDict"
        self.vertices = vertices
        self.blocks = [block for block in blocks if block.zone != "void"]

    def setInternalFaces(self) :
        for block in self.blocks :
            for face in block.faceList:
                for nbrBlock in self.blocks :
                    if block != nbrBlock :
                        for nbrFace in nbrBlock.faceList :
                            if face == nbrFace :
                                face.isInternal = True

    def writeHeader(self) :
        file = open(self.filename, "w")

        header = []
        header.append("/*--------------------------------*- C++ -*----------------------------------*\ \n")
        header.append("| ========                 |                                                 | \n")
        header.append("| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           | \n")
        header.append("|  \\    /   O peration     | Version:  5.0                                   | \n")
        header.append("|   \\  /    A nd           | Web:      www.OpenFOAM.org                      | \n")
        header.append("|    \\/     M anipulation  |                                                 | \n")
        header.append("\*---------------------------------------------------------------------------*/ \n")
        header.append("FoamFile\n")
        header.append("{\n")
        header.append("    version     5.0;\n")
        header.append("    format      ascii;\n")
        header.append("    class       dictionary;\n")
        header.append("    object      blockMeshDict;\n")
        header.append("}\n")
        header.append("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n")
        header.append("\n")
        header.append("convertToMeters 1;\n")
        header.append("\n")

        file.writelines(header)
        file.close()

    def writeVertices(self) :
        file = open(self.filename, "a")

        content = []
        content.append("vertices\n(\n")
        for vertex in self.vertices :
            content.append("    "+vertex.string()+" // "+str(vertex.label)+"\n")
        content.append(");\n\n")
        
        file.writelines(content)
        file.close()

    def writeBlocks(self) :
        file = open(self.filename, "a")
        content = []
        content.append("blocks\n(\n")
        for block in self.blocks :
            content.append("    "+block.string()+"\n")
        content.append(");\n\n")

        file.writelines(content)
        file.close()

    def writeEdges(self) :
        file = open(self.filename, "a")
        file.writelines(["edges\n","(\n",");\n\n"])
        file.close()

    def createPatches(self) :
        self.patches = []
        patchNames = []
        self.setInternalFaces()
        for block in self.blocks :
            print("Evaluating block ", block.label)
            for i, face in enumerate(block.faceList):
                if not face.isInternal :
                    if face.patchName not in patchNames :
                        if i < 4 + (len(block.faceList)-6) : # 0 => +x, 1 => -x, 2 => +z, 3 => -z, 4 => +y, 5 => -y
                            patchType = "wall"
                        else :
                            patchType = "wedge"
                        newPatch = patchClass(face.patchName, patchType, [])
                        newPatch.faceList.append(face)
                        self.patches.append(newPatch)
                        patchNames.append(face.patchName)
                    else :
                        for patch in self.patches :
                            if face.patchName == patch.patchName :
                                patch.faceList.append(face)

    def writePatches(self) :
        self.createPatches()
        file = open(self.filename, "a")

        content = []
        file.writelines(["boundary\n(\n"])
        for patch in self.patches :
            file.writelines(patch.strings())
            file.writelines(["\n"])
        file.writelines([");\n"])
        file.close()

    def writeMergePatchPairs(self) :
        file = open(self.filename, "a")
        file.writelines(["mergePatchPairs\n","(\n",");\n\n"])
        file.close()



class topoSetDictClass(blockMeshDictClass) :

    def __init__(self, bMD) :
        blockMeshDictClass.__init__(self, bMD.vertices, bMD.blocks)
        self.filename = "topoSetDict"
        self.faceSetNames = []

    def faceSetEntry(self, name, action, stringBB) :
        content = []
        indent = "    "
        content.append(indent+"{\n")
        content.append(indent+indent+"name "+str(name)+";\n")
        content.append(indent+indent+"type faceSet;\n")
        content.append(indent+indent+"action "+str(action)+";\n")
        content.append(indent+indent+"source boxToFace;\n")
        content.append(indent+indent+"sourceInfo\n")
        content.append(indent+indent+"{\n")
        content.append(indent+indent+indent+stringBB+"\n")
        content.append(indent+indent+"}\n")
        content.append(indent+"}\n")
        return content

    def faceZoneEntry(self, faceSetName) :
        content = []
        indent = "    "
        content.append(indent+"{\n")
        content.append(indent+indent+"name "+str(faceSetName)+";\n") #Set and Zone will share the same name
        content.append(indent+indent+"type faceZoneSet;\n")
        content.append(indent+indent+"action new;\n")
        content.append(indent+indent+"source setToFaceZone;\n")
        content.append(indent+indent+"sourceInfo\n")
        content.append(indent+indent+"{\n")
        content.append(indent+indent+indent+"faceSet "+faceSetName+";\n")
        content.append(indent+indent+"}\n")
        content.append(indent+"}\n")
        return content

    def writeHeader(self) :
        file = open(self.filename, "w")

        header = []
        header.append("/*--------------------------------*- C++ -*----------------------------------*\ \n")
        header.append("| ========                 |                                                 | \n")
        header.append("| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           | \n")
        header.append("|  \\    /   O peration     | Version:  5.0                                   | \n")
        header.append("|   \\  /    A nd           | Web:      www.OpenFOAM.org                      | \n")
        header.append("|    \\/     M anipulation  |                                                 | \n")
        header.append("\*---------------------------------------------------------------------------*/ \n")
        header.append("FoamFile\n")
        header.append("{\n")
        header.append("    version     5.0;\n")
        header.append("    format      ascii;\n")
        header.append("    class       dictionary;\n")
        header.append("    object      topoSetDict;\n")
        header.append("}\n")
        header.append("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n")
        header.append("\n")

        file.writelines(header)
        file.close()

    def writeFaceSets(self):
        file = open(self.filename, "a")
        baffleFaces = [] # To avoid duplicate faces on master and slave

        file.writelines(["actions\n","(\n"])

        for block in self.blocks :
            for face in block.faceList :
                if face.isBaffle :
                    if face.isInternal :
                        if face not in baffleFaces :
                            if face.patchName not in self.faceSetNames :
                                action = "new"
                                self.faceSetNames.append(face.patchName)
                            else :
                                action = "add"
                            file.writelines(self.faceSetEntry(face.patchName, action, face.stringBB()))
                            baffleFaces.append(face)
                    else :
                        sys.exit("External faces cannot be baffles! Error in faces of block "+str(block.label))        
        file.close()

    def writeFaceZones(self) :
        file = open(self.filename, "a")

        for faceSetName in self.faceSetNames :
            file.writelines(self.faceZoneEntry(faceSetName))
        file.writelines([");\n"])
        file.close()




###############################################################################
### FUNCTIONS
###############################################################################

def readFile(filename) :
    lines = []
    tmpLines = open(filename, "r").readlines()
    for line in tmpLines :
        line = re.split("\s", line)
        while '' in line:
            line.remove('')
        if len(line) > 0 :
            lines.append(line)
    return lines



def readDict() :
    global origin, wedgeAngle
    global blockTypeList
    
    lines = readFile("wedgeDict")

    # Read through dict
    for i in range(0, len(lines)) :

        if   lines[i][0] == "wedgeAngle" :
            wedgeAngle = (2*math.pi/360)*float(lines[i][1])

        elif lines[i][0] == "originCartesianCoordinates" :
            origin = vertexClass(lines[i][1], lines[i][2], lines[i][3])
        
        # Read blocks subdict        
        elif lines[i][0] == "blockList" :
            while True :
                i += 1
                if   lines[i][0] != "{" and lines[i][0] != "}" and lines[i][0] != "#":
                    label = lines[i][0]
                    i += 1
                    while True :
                        i += 1
                        if   lines[i][0] == "zone" :
                            zone = lines[i][1]
                        elif lines[i][0] == "deltaX" :
                            deltaX = float(lines[i][1])
                        elif lines[i][0] == "deltaZ" :
                            deltaZ = float(lines[i][1])
                        elif lines[i][0] == "nX" :
                            nX = int(lines[i][1])
                        elif lines[i][0] == "nZ" :
                            nZ = int(lines[i][1])
                        elif lines[i][0] == "posXFacePatchName" :
                            try:
                                if lines[i][2] == "baffle" :
                                    isBaffle = True
                                else :
                                    isBaffle = false
                            except:
                                isBaffle = False
                            fPosX = faceClass([], lines[i][1], isBaffle)
                        elif lines[i][0] == "negXFacePatchName" :
                            try:
                                if lines[i][2] == "baffle" :
                                    isBaffle = True
                                else :
                                    isBaffle = false
                            except:
                                isBaffle = False
                            fNegX = faceClass([], lines[i][1], isBaffle)
                        elif lines[i][0] == "posZFacePatchName" :
                            try:
                                if lines[i][2] == "baffle" :
                                    isBaffle = True
                                else :
                                    isBaffle = false
                            except:
                                isBaffle = False
                            fPosZ = faceClass([], lines[i][1], isBaffle)
                        elif lines[i][0] == "negZFacePatchName" :
                            try:
                                if lines[i][2] == "baffle" :
                                    isBaffle = True
                                else :
                                    isBaffle = false
                            except:
                                isBaffle = False
                            fNegZ = faceClass([], lines[i][1], isBaffle)

                        elif lines[i][0] == "}" :
                            blockTypeList.append(blockTypeClass(label, zone, deltaX, deltaZ, nX, nZ, [fPosX, fNegX, fPosZ, fNegZ]))
                            break
                    
                elif lines[i][0] == "}" :
                    break



def readLayout() :
    global blockTypeList, blockList, blockLayout
    
    layout = readFile("wedgeLayout")

    for row in range(0, len(layout)) :
        tmpRow = []
        for el in range(0, len(layout[row])) :
            try :
                blockType = [blockType for blockType in blockTypeList if blockType.label == str(layout[row][el])][0]
            except :
                sys.exit("Block type \""+str(layout[row][el])+"\" undefined in wedgeDict!")
            
            block = blockClass(blockType, row, el)
            tmpRow.append(block)
            blockList.append(block)

        blockLayout.append(tmpRow)



###############################################################################
### MAIN
###############################################################################

readDict()

readLayout()

y = origin.y
z = origin.z

for i in range(len(blockLayout)-1, -1, -1):
    row = blockLayout[i]
    x = origin.x
    for j in range (0, len(row)) :
        block = row[j]
        block.createVerticesAndFaces(x, y, z, vertexList)
        x += block.deltaX
    z += row[0].deltaZ

blockMeshDict = blockMeshDictClass(vertexList, blockList)

blockMeshDict.writeHeader()

blockMeshDict.writeVertices()

blockMeshDict.writeBlocks()

blockMeshDict.writeEdges()

blockMeshDict.writePatches()


for block in blockMeshDict.blocks :
    print("Block: "+block.label)
    for i, face in enumerate(block.faceList) :
        print(" | Face "+str(i)+" (patchName: ", face.patchName,") vertices: ")
        print(" | Face string: ", face.string())
        print(" | isInternal: ", face.isInternal)
        print(" | isBaffle: ", face.isBaffle)
        for vertex in face.vertexList :
            print(" | | Vertex label: "+str(vertex.label))


topoSetDict = topoSetDictClass(blockMeshDict)

topoSetDict.writeHeader()
topoSetDict.writeFaceSets()
topoSetDict.writeFaceZones()
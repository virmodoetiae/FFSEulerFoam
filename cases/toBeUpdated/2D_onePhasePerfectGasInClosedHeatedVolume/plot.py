'''
import time
import numpy as np
import matplotlib.pyplot as plt

def follow(thefile):
    thefile.seek(0,2) # Go to the end of the file
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1) # Sleep briefly
            continue
        yield line

i = 0

log = open("log", "r")

iters = []
residuals = []

for line in follow(log):
    if "p_rgh, Initial residual = " in line :
        residual = float(line.split()[7].split(",")[0])
        iters.append(i)
        residuals.append(residual)
        plt.semilogy(iters, residuals, linestyle='-')
        plt.pause(1e-1)
        if len(iters) == 100 :
            iters = []
            residuals = []
        i+=1
        
plt.show()
'''
###
'''

import time
import select
import subprocess
import matplotlib.pyplot as plt

f = subprocess.Popen(['tail','-F', "log"],\
        stdout=subprocess.PIPE,stderr=subprocess.PIPE)
p = select.poll()
p.register(f.stdout)

i = 0
iters = []
residuals = []
read = False
while True:
    if p.poll(1):
        line = f.stdout.readline().decode("utf-8")
        
        if "PIMPLE: iteration " in line :
            read = True
        if "p_rgh, Initial residual = " in line and read:
            print(line)
            residual = float(line.split()[7].split(",")[0])
            iters.append(i)
            residuals.append(residual)
            plt.semilogy(iters, residuals)
            plt.pause(1e-20)
            if len(iters) == 20 :
                iters = []
                residuals = []
            else :
                i+=1
            read = False
    time.sleep(1e-20)

#plt.show()
'''
import sys
import matplotlib.pyplot as plt

filename = sys.argv[1]
file = open(filename, "r")

try:
    initialResidualsOnlyOnFinalPIMPLE = bool(sys.argv[2])
except :
    initialResidualsOnlyOnFinalPIMPLE = True




i = 0
iters = []
initialResiduals = []

read = False
lines = file.readlines()

n = 0;
if initialResidualsOnlyOnFinalPIMPLE :
    for line in lines :
        if "PIMPLE: iteration" in line :
            nn = int(line.split()[2])
            if nn > n :
                n = nn
            else :
                break
print(n)
for line in lines :
    if "PIMPLE: iteration "+str(n) in line :
        read = True
    if read and ("p_rgh, Initial residual = " in line) : 
        residual = float(line.split()[7].split(",")[0])
        iters.append(i)
        initialResiduals.append(residual)
        read = False
        i += 1

plt.semilogy(iters, initialResiduals)
plt.show()
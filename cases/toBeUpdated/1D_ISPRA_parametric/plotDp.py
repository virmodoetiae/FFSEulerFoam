### IMPORTS

import sys
import matplotlib.pyplot as plt

### FUNCTIONS

### MAIN

for q in range(len(sys.argv)-1) :

    # Read log lines
    casename = str(sys.argv[q+1])
    filename = casename+"/postProcessing/pressureDrop_IO/0/pressureDrop.dat"
    file = open(filename, "r")
    lines = file.readlines()

    # Read time, boiling inception time, power off time
    times = []
    deltaPs = []
    for line in lines :
        if "Time = " in line :
            times.append(float(line.split()[2]))
        elif "deltaP = " in line :
            deltaPs.append(float(line.split()[2]))
    if len(times) > len(deltaPs) :
        del times[-1]

    plt.plot(times, deltaPs, label=casename)
    plt.grid(True)
    plt.legend()

plt.show()

### IMPORTS

import os
import sys
import psutil
import shutil
import subprocess

### USER SELECRABLE

# Velocities for which to run the transients
us = [5, 4, 3.5, 3.25, 3, 2.8, 2.6, 2.4, 2.2, 2, 1.9, 1.8, 1.7, 1.6, 1.5]

#
rootCaseName = "rootCase"

### GLOBALS

# Dictionary that related process PID to caseName
pidToName = {}

rootPath = os.getcwd()

### FUNCTIONS

def checkForFinishedProcesses(processes) :
    
    for process in processes :
        N = process.poll()
        if N != None :
            if N < 0 :
                print("Case "+pidToName[process.pid]+" finished with errors!")
            else :
                print("Case "+pidToName[process.pid]+" finished successfully!")
            processes.remove(process)
            process.kill()

def waitForAllProcesses(processes) :

    while len(processes) > 0 :
        checkForFinishedProcesses(processes)

def CPUsInUse() :
    
    return int(psutil.cpu_percent(interval=0.2)*psutil.cpu_count()/100);

# If the subprocess that is being run uses more CPUs, pass that number as 
# maxCPUsPerProcess
def waitForCPUsAndCheckForFinishedProcesses(processes, maxCPUs, maxCPUsPerProcess=1) :
    
    wait = True
    while wait :
        if CPUsInUse() < maxCPUs-maxCPUsPerProcess :
            wait = False
    checkForFinishedProcesses(processes)

### MAIN

rootCasePath = rootPath+"/"+rootCaseName+"/"
maxCPUs = psutil.cpu_count()/2 # Div by 2 due to hyperthreading
processes = []

# For every case
for u in us :

    # Define case variables
    caseName = "u"+str(u)
    casePath = rootPath+"/"+caseName+"/"
    caseLog = casePath+"log_"+caseName
    
    # Copy folder
    shutil.copytree(rootCasePath, casePath)
    
    # This runs the changeU.sh bash script passing caseName and str(u) as 
    # arguments to change inlet velocity (see changeU.sh)
    changeUProcess = subprocess.run(['./changeU.sh', caseName, str(u)], stdout=subprocess.DEVNULL)
    
    waitForCPUsAndCheckForFinishedProcesses(processes, maxCPUs)

    print("Running case: "+caseName+" | CPUs in use = ", CPUsInUse())

    with open(caseLog, "w") as log :
        process = subprocess.Popen(['FFSEulerFoam', '-case', caseName], stdout=log, stderr=log)
        processes.append(process)
        pidToName[process.pid] = caseName

waitForAllProcesses(processes)

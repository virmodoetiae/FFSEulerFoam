#!/bin/sh
cd ${0%/*} || exit 1    # Run from this directory

# Source tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

foamDictionary $1/0/U.liquid -entry internalField -set "uniform (0 0 $2);"
foamDictionary $1/0/U.liquid -entry boundaryField.inlet.value -set "uniform (0 0 $2);"

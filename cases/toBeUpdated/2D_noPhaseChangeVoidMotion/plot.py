import sys
import matplotlib.pyplot as plt

fig, (ax1, ax2) = plt.subplots(2, sharex=True)

ax1.set_ylabel("Residual (-)")
ax2.set_ylabel("# pEqn iters per time step(-)")
ax2.set_xlabel("Time (s)")

for q in range(len(sys.argv)-1) :

    filename = sys.argv[q+1]
    file = open(filename, "r")

    #try:
        #initialResidualsOnlyOnFinalPIMPLE = bool(sys.argv[2])
    #except :
    
    initialResidualsOnlyOnFinalPIMPLE = True

    i = 0
    iters = []
    initialResiduals = []

    read = False
    lines = file.readlines()

    n = 0;
    if initialResidualsOnlyOnFinalPIMPLE :
        for line in lines :
            if "PIMPLE: iteration" in line :
                nn = int(line.split()[2])
                if nn > n :
                    n = nn
                else :
                    break

    pEqnIters = 0
    pEqnIterss = []
    executionTimes = [0]
    times = []
    for line in lines :
        if "Time = " in line :
            splitLine = line.split()
            if len(splitLine) == 3 :
                times.append(float(splitLine[2]))
        if "GAMG:  Solving for p_rgh" in line :
            pEqnIters += int(line.split()[14])
        if "ExecutionTime = " in line :
            executionTimes.append(float(line.split()[2]))
        if "PIMPLE: iteration "+str(n) in line :
            pEqnIterss.append(pEqnIters)
            pEqnIters = 0
            read = True
        if read and ("p_rgh, Initial residual = " in line) : 
            residual = float(line.split()[7].split(",")[0])
            iters.append(i)
            initialResiduals.append(residual)
            read = False
            i += 1

    mergeFactor = 10
    timeStepDurations = []
    i = 0
    while i < len(executionTimes)-1 :
        try :
            deltaT = (executionTimes[i+mergeFactor]-executionTimes[i])/mergeFactor
            for j in range(mergeFactor) :
                timeStepDurations.append(deltaT)
                i += 1
        except: 
            i += 1
            pass

    adjustedTimeStepDurations = []

    for i in range(len(iters)) :
        try :
            adjustedTimeStepDurations.append(timeStepDurations[i])
        except :
            adjustedTimeStepDurations.append(adjustedTimeStepDurations[-1])

    while len(times) > len(initialResiduals) :
        del times[-1]

    ax1.semilogy(times, initialResiduals, label=filename)
    ax2.plot(times, pEqnIterss, label=filename)

ax1.legend()
ax2.legend()
ax1.grid(True)
ax2.grid(True)
plt.show()
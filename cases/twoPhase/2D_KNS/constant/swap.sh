#!/bin/bash

FILE=phaseProperties
FILE_T=phasePropertiesT
FILE_F=phasePropertiesF

if test -f "$FILE_T"; then
    mv "phaseProperties" "$FILE_F"
    mv "$FILE_T" "phaseProperties" 
    echo "Ready for THYAC"
else
	if test -f "$FILE_F"; then
		mv "phaseProperties" "$FILE_T"
    	mv "$FILE_F" "phaseProperties" 
    	echo "Ready for FFSEulerFoam"
	fi
fi
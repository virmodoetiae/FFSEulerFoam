/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v2006                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
FoamFile
{
    version         2;
    format          ascii;
    class           dictionary;
    location        "system";
    object          fvSolution;
}

solvers
{
    p_rgh
    {
        solver          GAMG;
        smoother        DIC;
        tolerance       1e-06;
        relTol          0;
    }
    p_rghFinal
    {
        solver          GAMG;
        smoother        DIC;
        tolerance       1e-06;
        relTol          0;
    }
    "e.*"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-06;
        relTol          0;
        minIter         1;
    }
    "h.*"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-06;
        relTol          0;
        minIter         1;
    }
    ".*"
    {
        solver          PBiCGStab;
        preconditioner  diagonal;
        tolerance       1e-06;
        relTol          0.001;
    }
    alpha
    {
        solver          MULES;
        adjustSubCycles true;
        alphaMaxCo      0.25;
        solvePhase      "vapour";
    }
}

PIMPLE
{
    nOuterCorrectors                    9;
    nCorrectors                         12;
    correctUntilConvergence             true;
    nNonOrthogonalCorrectors            0;
    partialEliminationMode              implicit;
    momentumMode                        faceCentered;
    massTransferSafetyFactor            0.40;
    enthalpyStabilizationMode           source;
    maxTInterfaceDdt                    1e4; // K/s
    continuityErrorCompensationMode     Su;
    continuityErrorScaleFactor          0.9;

    minKdFF                             1;
    maxKdFF                             1e5;
    maxAlphaKdFF                        1e-3;
    
    residualControl
    {
        p_rgh
        {
            tolerance                           1e-05;
            relTol                              0;
            useFirstPISOInitialResidual         true;
        }
        h.liquid
        {
            tolerance       1e-06;
            relTol          0;
        }
    }
}

relaxationFactors
{
    equations
    {
        ".*"                1;
        "h.vapour"          0.1;
        "h.vapourFinal"     0.1;
        "h.liquid"          0.75;
    }
    fields
    {
        //"latentHeat"                0.01;
        //"latentHeatFinal"           0.01;
        "T.interface"               0.25;
        "T.interfaceFinal"          0.25;
        //"regime"                    0.25;
        //"regimeFinal"               0.25;
        //"htc.liquid.vapour"         0.05;
        //"htc.liquid.vapourFinal"    0.05;
        
        //"htc.liquid.structure"      0.25;
        "dmdt"                      0.25;
        "dmdtFinal"                 0.25;
        "dmdt.*"                    0.25;
    }
}


// ************************************************************************* //

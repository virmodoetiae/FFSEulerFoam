#include "createFvOptions.H"

autoPtr<thermalHydraulicsModel> THY
(
    thermalHydraulicsModel::New
    (
    	runTime,
        mesh, 
        pimple, 
        fvOptions
    )
);

//- Update Courant for setInitialDeltaT.H
THY->correctCourant();

/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2016 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    plotOverLine

Description
    Return the values of a volScalarField along a line that connects two 
    points within the computational domain. The field values are reported
    against the normalized curvilinear coordinate (0-1) that connects the
    two point and the coordinate at which each cell center value is reported
    is the projected cell-center curvilinear coordinate (can't get any better
    than this)

    Entries example of plotOverLineDict. Said dict can have multiple entries
    
    plotOverLineDict

    operation1
    {
        //- Field name
        field           "fieldName";

        //- Time folder of the field to be plotted
        time            2;

        //- Start point
        p0              (0.5 0 0.05);

        //- End point
        p1              (0.75 1.0 0.05);

        //- This sampling parameter is not mandatory but can speed up the
        //  utility on larger meshes if adequately chosen
        
        nSamples        1000;   //- Defaults to number of cells in mesh
    }

    operation2
    {
        ...
    }

    ...

Author
    Stefan Radman (stefan.radman@epfl.ch; stefanradman92@gmail.com)

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "meshSearch.H"
#include "zeroGradientFvPatchFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void writeVector(std::ofstream& file, const word& keyword, const vector& v)
{
    file << keyword << ": ( ";
    forAll(v, i)
    {
        file<< v[i] << " ";
    }
    file << ")" << std::endl;
}

int main(int argc, char *argv[])
{
    #include "addRegionOption.H"
    //#include "addTimeOptions.H"
    #include "setRootCase.H"
    #include "createTime.H"
    instantList Times = runTime.times();
    //#include "checkTimeOptions.H"
    //runTime.setTime(Times[startTime], startTime);
    #include "createNamedMesh.H"

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    // Load dict
    IOdictionary dict
    (
        IOobject
        (
            "plotOverLineDict",
            runTime.system(),
            mesh,
            IOobject::MUST_READ_IF_MODIFIED,
            IOobject::NO_WRITE
        )
    );

    //- Cell center vectors
    const volVectorField& C(mesh.C());

    //- Mesh search object for handling boundary values
    meshSearch ms(mesh, polyMesh::FACE_PLANES);

    forAll(dict.toc(), i)
    {
        //- Load subDict
        word itemName(dict.toc()[i]);
        if (!dict.isDict(itemName)) continue;
        const dictionary& itemDict(dict.subDict(itemName));
        Info << "Processing dict " << itemName << endl;

        //- Set time, if possible. Otherwise, this dict entry is skipped
        //  entirely
        scalar time(itemDict.get<scalar>("time"));
        bool timeNotFound(true);
        forAll(Times, i)
        {
            const Instant<word> timeInstant(Times[i]);
            if (time == timeInstant.value())
            {
                runTime.setTime(Times[i], i);
                timeNotFound = false;
            }
        }
        if (timeNotFound)
        {
            Info << "    - time " << time << " not found!" << endl;
            continue;
        }
        else
        {
            Info << "    - time = " << time << endl;
        }

        //- Read start and end points of plotting line
        vector p0(itemDict.get<vector>("p0"));
        vector p1(itemDict.get<vector>("p1"));
        vector deltaP(p1-p0);
        scalar magDeltaP(mag(p1-p0));
        vector deltaPNorm(deltaP/magDeltaP);

        //- Load field to be processed
        word fieldName(itemDict.lookup("field"));
        Info << "    - field = " << fieldName << endl;
        volScalarField field
        (
            IOobject
            (
                fieldName,
                runTime.timeName(),
                mesh,
                IOobject::MUST_READ,
                IOobject::AUTO_WRITE
            ),
            mesh
        );

        //- Read sampling parameters
        int nSamples
        (
            max(itemDict.lookupOrDefault<int>("nSamples", C.size()),2)
        );
        Info << "    - nSamples = " << nSamples << endl;
        bool includeBoundaryValues
        (
            itemDict.lookupOrDefault<bool>
            (   
                "includeBoundaryValues", 
                true
            )
        );
        Info<< "    - includeBoundaryValues = " << includeBoundaryValues 
            << endl;

        //- Read and project boundary field values along p0-p1 axis
        scalarList boundaryCurvilinearCoordinates(0);
        scalarList boundaryCurvilinearField(0);
        vector p0Star(p0);
        scalar s0Star(0);      
        scalar prevS(-1);
        List<pointIndexHit> boundaryIntersectionPoints
        (
            ms.intersections(p0, p1)
        );
        forAll(boundaryIntersectionPoints, i)
        {
            const pointIndexHit& boundaryIntersectionPoint
            (
                boundaryIntersectionPoints[i]
            );
            if (boundaryIntersectionPoint.hit())
            {
                const point& p(boundaryIntersectionPoint.hitPoint());
                scalar s = (mag(p-p0)/magDeltaP);
                if (i == 0)
                {
                    p0Star = p;
                    s0Star = s;
                }
                if (includeBoundaryValues)
                {
                    if (s != prevS)
                    {
                        label patchi(mesh.boundaryMesh().whichPatch
                        (
                            boundaryIntersectionPoint.index())
                        );
                        label facei
                        (
                            boundaryIntersectionPoint.index()
                        -   mesh.boundary()[patchi].start()
                        );
                        const scalar& v(field.boundaryField()[patchi][facei]);

                        boundaryCurvilinearCoordinates.append(s);
                        boundaryCurvilinearField.append(v);

                        prevS = s;
                    }
                }
            }
        }

        //- Read and project internal field values along p0-p1 axis
        scalarList internalCurvilinearCoordinates(0);
        scalarList internalCurvilinearField(0);
        scalar ds(1.0/(nSamples-1));
        scalar s(0);
        int prevIndex(-2);
        while (s < 1.0)
        {
            vector p(p0+s*deltaP);
            /*
            int index(-1);
            scalar minMagDeltaCP(1e69);
            forAll(C, j)
            {
                scalar magDeltaCP(mag(C[j]-p));
                if (magDeltaCP < searchRadius and magDeltaCP < minMagDeltaCP)
                {
                    minMagDeltaCP = magDeltaCP;
                    index = j;
                }
            }
            */

            //- Find the index by using the findCell function sparingly 
            //  (pointInCell is much faster than findCell and establishes a
            //  criterion wether findCell should be used at all or not for the
            //  current p being processed)
            label index(-1);
            bool inPrevCell(false);
            if (prevIndex != -2)
                inPrevCell = 
                    mesh.pointInCell(p, prevIndex, polyMesh::FACE_PLANES);
            if (!inPrevCell)
            {
                index = ms.findCell(p);
            }
            
            if (index != -1)
            {
                if (index != prevIndex)
                {
                    vector deltaCP0(C[index]-p0);
                    scalar magDeltaCP0(mag(deltaCP0));
                    scalar cosCP0P1
                    (
                        (deltaCP0 & deltaP)/
                        (magDeltaCP0*magDeltaP)
                    );
                    s = cosCP0P1*(magDeltaCP0/magDeltaP);
                    internalCurvilinearCoordinates.append(s);
                    internalCurvilinearField.append(field[index]);
                    prevIndex = index;
                }
            }

            s += ds;
        } 

        //- Merge boundary and internal field values
        scalarList curvilinearCoordinates(0);
        scalarList curvilinearField(0);
        int minJ(0);
        int lastB(boundaryCurvilinearCoordinates.size()-1);
        int lastI(internalCurvilinearCoordinates.size()-1);
        forAll(internalCurvilinearCoordinates, i)
        {
            const scalar& si(internalCurvilinearCoordinates[i]);
            for(int j = minJ; j < boundaryCurvilinearCoordinates.size(); j++)
            {
                const scalar& sj(boundaryCurvilinearCoordinates[j]);
                if (sj < si)
                {
                    curvilinearCoordinates.append(sj);
                    curvilinearField.append(boundaryCurvilinearField[j]);
                    minJ += 1;
                }
            }
            curvilinearCoordinates.append(si);
            curvilinearField.append(internalCurvilinearField[i]);
        }
        if (lastB != -1)
        {
            if 
            (
                boundaryCurvilinearCoordinates[lastB] > 
                internalCurvilinearCoordinates[lastI]
            )
            {
                curvilinearCoordinates.append
                (
                    boundaryCurvilinearCoordinates[lastB]
                );
                curvilinearField.append
                (
                    boundaryCurvilinearField[lastB]
                );
            }
        }
        
        //- Rescaling curvilinear coordinates to range of p0-p1 line that lies
        //  entirely within the mesh
        scalarList adjustedCurvilinearCoordinates(0);
        int N(curvilinearCoordinates.size()-1);
        scalar d0
        (
            (s0Star < curvilinearCoordinates[0]) ?
            mag(p0Star-p0) :
            curvilinearCoordinates[0]*magDeltaP
        );
        scalar d1(curvilinearCoordinates[N]*magDeltaP);
        scalar deltaD(d1-d0);
        forAll(curvilinearCoordinates, i)
        {
            adjustedCurvilinearCoordinates.append
            (
                (curvilinearCoordinates[i]*magDeltaP-d0)/deltaD
            );
        }

        //- Save output to file
        word filename
        (
            "plotOverLine_"
        +   itemName
        +   ".out"
        );
        std::ofstream file(filename);
        file << "Time: " << runTime.timeName() << std::endl;
        file << "Field: " << fieldName << std::endl;
        writeVector(file, "Line axis", ((p1-p0)/magDeltaP));
        vector startPoint
        (
            (s0Star < curvilinearCoordinates[0]) ?
            p0Star :
            p0+deltaP*curvilinearCoordinates[0]
        );
        writeVector
        (
            file, 
            "Start point", 
            startPoint
        );
        file << std::endl;
        file << "Distance | value" << std::endl;
        forAll(adjustedCurvilinearCoordinates, i)
        {
            scalar d(adjustedCurvilinearCoordinates[i]*deltaD);
            file<< d << " " 
                << curvilinearField[i] 
                << std::endl;
        }
        file.close();

        Info << "    -> done!" << endl << endl;
    }
    
    return 0;
}


// ************************************************************************* //
